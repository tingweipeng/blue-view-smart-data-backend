// 放置全局指令
import store from '@/store'
import Vue from 'vue'

// 全局的自定义执行 使用的时候需要使用 v-permission
Vue.directive('permission', {
  // el 指的是 被插入的dom元素
  // binding 对象 binding.value 获取得到 自定义指令的值 v-xxx="'abcd'"
  inserted(el, binding) {
    console.log(binding.value)
    // console.log(store.state.menu.menus)
    const menus = store.state.menu.menus
    // console.log(menus, 1111)
    if (!menus.includes(binding.value)) {
      // el.style.display = 'none'
      el.remove()
    }
  }
})
