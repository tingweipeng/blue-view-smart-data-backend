import Vue from 'vue'
import Vuex from 'vuex'
// 导入user模块
import user from './modules/user'
// 导入menu模块
import menu from './modules/menu'
Vue.use(Vuex)

const store = new Vuex.Store({
  // 组合模块的配置项
  modules: {
    user,
    menu
  }
})

export default store
