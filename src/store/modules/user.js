import { loginAPI } from '@/api/user'
import { getToken, removeToken, setToken } from '@/utils/auth'
// 存储组件里面需要共享的数据
const state = () => {
  return {
    token: getToken() || ''
  }
}
// 修改 state 里面的数据
const mutations = {
  // 修改state里面的 token
  updateToken(state, token) {
    state.token = token
    // 将token字符串存储到 cookie中
    setToken(token)
  },
  // 清空state里面的token
  deleteToken(state) {
    state.token = ''
    // 将 cookie 清空
    removeToken()
  }
}

// 发送异步请求
const actions = {
  async doLogin(cxt, data) {
    const res = await loginAPI(data)
    // console.log(res)
    cxt.commit('updateToken', res.data.token)
  }
}

export default {
  // 开启命名空间
  namespaced: true,
  state,
  mutations,
  actions
}
