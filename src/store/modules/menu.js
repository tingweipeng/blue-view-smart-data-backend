import { getProfileAPI } from '@/api/user'
import { routes, resetRouter } from '@/router'

export default {
  namespaced: true,
  state: () => ({
    menus: [], // 存储用户的权限
    allRoutes: [] // 保存所有的路由规则 包含静态路由规则与动态路由规则
  }),
  mutations: {
    updateMenus(state, data) {
      state.menus = data
    },
    updateAllRoutes(state, data) {
      state.allRoutes = [...routes, ...data]
    },
    // 清空所有的路由规则
    clearAllRouters(state) {
      state.allRoutes = []
      // 调用 resertRouter方法
      resetRouter()
    }
  },
  actions: {
    // cxt 代表store对象
    async getProfile(cxt) {
      const res = await getProfileAPI()
      // console.log(res, '11111')
      // 调用mutations的方法
      cxt.commit('updateMenus', res.data.permissions)
      return res.data.permissions
    }
  },
  getters: {}
}
