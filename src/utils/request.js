import axios from 'axios'
import { getToken } from './auth'
import router from '@/router'
import store from '@/store'
const service = axios.create({
  baseURL: 'https://api-hmzs.itheima.net/v1',
  timeout: 5000 // request timeout
})

// 添加请求拦截器
service.interceptors.request.use(function(config) {
  // 在发送请求之前做些什么
  // 1. 获取token getToken获取
  const token = getToken()
  // 2. 判断 token 是否存在
  if (token) {
    // config 请求配置对象
    config.headers.Authorization = token
  }
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response.data
}, function(error) {
  // token失效的情况
  if (error.response.status === 401) {
    // 清除token
    // store.commit('模块名/函数')
    store.commit('user/deleteToken')
    // 跳转到登录页面
    router.push('/login')
  }
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error)
})

export default service
