// 权限控制
import router from './router'
import store from './store'
import { getToken } from './utils/auth'
// 导入 动态路由规则
import { asyncRoutes } from '@/router/asyncRoutes'
// console.log(asyncRoutes, '动态路由规则')
// 从 pers 数组中 将 一级路由权限标识筛选出来
function hFirstPers(pers) {
  const fPers = pers.map((item) => item.split(':')[0])
  return [...new Set(fPers)]
}
// 从 pers 数组中 将 二级路由权限标识筛选出来
function hSecondPers(pers) {
  const sPers = pers.map(
    (item) => `${item.split(':')[0]}:${item.split(':')[1]}`
  )
  return [...new Set(sPers)]
}
// 根据 一级路由标识和二级路由标识 去 动态路由数组里面 过滤 对应的权限标识
function handleFilterRoutes(firstPers, secondPers, asyncRoutes) {
  // 处理超级管理员
  // console.log(firstPers) [*:*:*]
  // ["*"]
  if (firstPers.includes('*')) {
    return asyncRoutes
  }
  // 从 动态路由数组里面 筛选一级路由规则
  const fRourts = asyncRoutes.filter((item) => {
    return firstPers.includes(item.permission)
  })
  // 从 fRourts 里面 得到我们需要的数组
  const resRoutes = fRourts.map((item) => {
    return {
      ...item,
      children: item.children.filter((child) =>
        secondPers.includes(child.permission)
      )
    }
  })
  return resRoutes
}

// 路由前置导航守卫
// to 表示去哪里
// from 来自哪里
// next()
const WHITE_LIST = ['/login', '/404']
router.beforeEach(async(to, from, next) => {
  // 获取token
  const token = getToken()
  // 判断有没有token
  if (token) {
    // 放行
    next()
    // 调用 store actions里面的方法
    // this.$store.dispatch('模块名/函数名')
    const pers = await store.dispatch('menu/getProfile')
    // console.log(pers, '1111')
    const firstPers = hFirstPers(pers)
    // console.log(firstPers)
    const secondPers = hSecondPers(pers)
    // console.log(secondPers)
    // 调用函数 根据 一级路由标识和二级路由标识 去 动态路由数组里面 过滤 对应的权限标识
    const filterRoutes = handleFilterRoutes(firstPers, secondPers, asyncRoutes)
    // console.log('过滤标识', filterRoutes)
    // 将处理后的路由规则添加到路由对面里面
    filterRoutes.forEach((item) => {
      router.addRoute(item)
    })
    // 将 处理后的路由规则与静态路由规则合到一起
    store.commit('menu/updateAllRoutes', filterRoutes)
  } else {
    // 表示没有token
    // 判断是不是要去白名单页面
    if (WHITE_LIST.includes(to.path)) {
      next()
    } else {
      // 跳转到登录页面
      next('/login')
    }
  }
})
