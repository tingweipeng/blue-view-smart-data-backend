import request from '@/utils/request'

/**
 * 查看计费规则列表
 * @param {*} params { page, pageSize }
 * @returns Promise对象
 */
export const getRuleListAPI = params => {
  return request({
    url: '/parking/rule/list',
    method: 'get',
    params
  })
}

/**
 * 添加计费规则
 * @param {*} data
 * @returns Promise对象
 */
export const addRuleAPI = data => {
  return request({
    url: '/parking/rule',
    method: 'post',
    data
  })
}

/**
 * 删除计费规则
 * @param {*} id
 * @returns Promise对象
 */
export const deleteRuleAPI = id => {
  return request({
    url: `/parking/rule/${id}`,
    method: 'DELETE'
  })
}

/**
 * 查看计费规则详情
 * @param {*} id
 * @returns Promise对象
 */
export const getRuleDetailAPI = id => {
  return request({
    url: `/parking/rule/${id}`,
    method: 'get'
  })
}

/**
 * 编辑计费规则
 * @param {*} data
 * @returns Promise对象
 */
export const updateRuleDetailAPI = data => {
  return request({
    url: '/parking/rule',
    method: 'put',
    data
  })
}
