// 导入 request
import request from '@/utils/request'

/**
 * 获取园区管理-企业列表
 * @param {*} params { name, page, pageSize }
 * @returns Promise对象
 */
export const getEnterpriseListAPI = params => {
  return request({
    url: '/park/enterprise',
    method: 'get',
    params
  })
}

/**
 * 园区管理-查询企业所属行业列表
 * @returns Promise对象
 */
export const getIndustryListAPI = () => {
  return request({
    url: '/park/industry'
  })
}

/**
 * 上传接口
 * @param {*} data { file type }
 * @returns Promise对象
 */
export const uploadAPI = data => {
  return request({
    url: '/upload',
    method: 'post',
    data
  })
}

/**
 * 园区管理-增加企业
 * @param {*} data
 * @returns Promise对象
 */
export const addEnterPriseAPI = data => {
  return request({
    url: '/park/enterprise',
    method: 'post',
    data
  })
}

/**
 * 园区管理-获取企业的详情
 * @param {*} id
 * @returns Promise对象
 */
export const getEnterPriseDetailAPI = id => {
  return request({
    url: `/park/enterprise/${id}`
  })
}

/**
 * 园区管理-编辑企业
 * @param {*} data
 * @returns Promise对象
 */
export const updateEnterPriseDetailAPI = data => {
  return request({
    url: '/park/enterprise',
    method: 'put',
    data
  })
}

/**
 *园区管理-删除企业
 * @param {*} id
 * @returns Promise对象
 */
export const delEnterPriseAPI = id => {
  return request({
    url: `/park/enterprise/${id}`,
    method: 'DELETE'
  })
}

/**
 * 园区管理-查询可租赁楼宇
 * @returns Promise对象
 */
export const getBuildingAPI = () => {
  return request({
    url: '/park/rent/building'
  })
}

/**
 * 园区管理-添加/续租企业的租赁合同
 * @returns Promise对象
 */
export const addRentAPI = data => {
  return request({
    url: '/park/enterprise/rent',
    method: 'post',
    data
  })
}

/**
 * 园区管理-企业租赁信息列表-展开查看
 * @param {*} id 企业id
 * @returns Promise对象
 */
export const getRentEnterPriseListAPI = id => {
  return request({
    url: `/park/enterprise/rent/${id}`
  })
}

/**
 * 园区管理-退租租赁合同
 * @param {*} id
 * @returns Promise对象
 */
export const exitRentAPI = id => {
  return request({
    url: `/park/enterprise/rent/${id}`,
    method: 'put'
  })
}

/**
 * 园区管理-删除企业的租赁合同
 * @param {*} id
 * @returns Promise对象
 */
export const delRentAPI = id => {
  return request({
    url: `/park/enterprise/rent/${id}`,
    method: 'DELETE'
  })
}
