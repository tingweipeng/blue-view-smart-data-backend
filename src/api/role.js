import request from '@/utils/request'

/**
 * 系统管理-角色管理-查询所有角色
 * @returns Promise对象
 */
export const getSysRoleAPI = () => {
  return request({
    url: '/park/sys/role'
  })
}

/**
 * 系统管理-角色管理-查询所有功能权限(树形)
 * @returns Promise对象
 */
export const getSysRoleTreeAPI = () => {
  return request({
    url: '/park/sys/permision/all/tree'
  })
}

/**
 * 系统管理-角色管理-查询当前角色详情-权限和分配人数
 * @param {*} id 角色id
 * @returns Promise对象
 */
export const getSysRoleDetailAPI = id => {
  return request({
    url: `/park/sys/role/${id}`
  })
}

/**
 * 系统管理-角色管理-角色关联的用户列表
 * @param {*} id 角色id
 * @returns Promise对象
 */
export const getUserRoleListAPI = id => {
  return request({
    url: `/park/sys/roleUser/${id}`
  })
}

/**
 * 系统管理-角色管理-添加角色
 * @param {*} data { roleName, remark, perms }
 * @returns Promise对象
 */
export const addRoleAPI = data => {
  return request({
    url: '/park/sys/role',
    method: 'post',
    data
  })
}

/**
 * 删除指定的角色
 * @param {*} roleId
 * @returns Promise对象
 */
export const delRoleUserAPI = (roleId) => {
  return request({
    url: `/park/sys/role/${roleId}`,
    method: 'DELETE'
  })
}

/**
 * 系统管理-角色管理-修改角色
 * @param {*} data
 * @returns Promise对象
 */
export const updateRoleUserAPI = data => {
  return request({
    url: '/park/sys/role',
    method: 'put',
    data
  })
}

/**
 * 获取员工列表
 * @param { page, pageSize} params
 * @returns
 */
export function getEmployeeListAPI(params) {
  return request({
    url: '/park/sys/user',
    params
  })
}

/**
 * 添加员工
 * @returns
 */
export function createEmployeeAPI(data) {
  return request({
    url: `/park/sys/user`,
    method: 'POST',
    data
  })
}

/**
 * 删除员工
 * @returns
 */
export function delEmployeeAPI(id) {
  return request({
    url: `/park/sys/user/${id}`,
    method: 'DELETE'
  })
}
