// 导入 request
import request from '@/utils/request'

/**
 * 查看月卡信息列表
 * @param {*} params {page,pageSize,carNumber,personName,cardStatus}
 * @returns promise对象
 */
export const getCardListAPI = params => {
  return request({
    url: '/parking/card/list',
    method: 'get',
    params
  })
}

/**
 * 添加月卡
 * @param {*} data
 * @returns Promise对象
 */
export const addMonthCardAPI = data => {
  return request({
    url: '/parking/card',
    method: 'post',
    data
  })
}

/**
 * 获取编辑页月卡详情
 * @param {*} id
 * @returns Promise对象
 */
export const getMonthCardDetailAPI = id => {
  return request({
    url: `/parking/card/detail/${id}`
  })
}

/**
 *  编辑月卡
 * @param {*} data
 * @returns Promise对象
 */
export const updateMonthCardAPI = data => {
  return request({
    url: '/parking/card/edit',
    method: 'put',
    data
  })
}

/**
 * 删除月卡（单个,批量）
 * @param {*} ids 批量删除时，id用逗号隔开
 * @returns Promise对象
 */
export const deleteMonthCardAPI = ids => {
  return request({
    url: `/parking/card/${ids}`,
    method: 'delete'
  })
}
